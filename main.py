import requests
from pydantic import BaseModel, ValidationError
from typing import List


class Weather(BaseModel):
    id: int
    main: str
    description: str
    icon: str


class Coord(BaseModel):
    lon: float
    lat: float


class MainW(BaseModel):
    temp: float
    feels_like: float
    temp_min: float
    temp_max: float
    pressure: float
    humidity: int
    sea_level: float = None
    grnd_level: float = None


class Wind(BaseModel):
    speed: float
    deg: int
    gust: float = None


class Clouds(BaseModel):
    all:  int


class Rain(BaseModel):
    h1: float = None
    h3: float = None


class Snow(BaseModel):
    h1: float = None
    h3: float = None


class Sys(BaseModel):
    type: int
    id: int
    message: float = None
    country: str
    sunrise: int
    sunset: int


class CityWeather(BaseModel):
    coord: Coord
    weather: List[Weather]
    base: str
    main: MainW
    visibility: int
    wind: Wind
    clouds: Clouds
    rain: Rain = None
    snow: Snow = None
    dt: int
    sys: Sys
    timezone: int
    id: int
    name: str
    cod: int


response = requests.get("http://api.openweathermap.org/data/2.5/weather?q=Lyubertsy&appid=962b165438a79a9361b5e33cf06ef"
                        "76f")


# input_weather = response.json()


'''input_weather = """
{
  "coord": {
    "lon": -122.08,
    "lat": 37.39
  },
  "weather": [
    {
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01d"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 282.55,
    "feels_like": 281.86,
    "temp_min": 280.37,
    "temp_max": 284.26,
    "pressure": 1023,
    "humidity": 100
  },
  "visibility": 16093,
  "wind": {
    "speed": 1.5,
    "deg": 350
  },
  "clouds": {
    "all": 1
  },
  "dt": 1560350645,
  "sys": {
    "type": 1,
    "id": 5122,
    "message": 0.0139,
    "country": "US",
    "sunrise": 1560343627,
    "sunset": 1560396563
  },
  "timezone": -25200,
  "id": 420006353,
  "name": "Mountain View",
  "cod": 200
}
"""
'''

try:
    city_weather = CityWeather.parse_raw(response.content)
except ValidationError as e:
    print("Exception", e.json())
else:
    print(city_weather)
